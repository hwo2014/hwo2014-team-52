{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.Text (Text)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson (decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Data.Aeson.Types (Parser)
import Data.String (IsString(..))
import Network.Socket (HostName)

import GameInitModel
import CarPositionsModel

type ClientMessage = String

joinMessage :: String -> String -> String
joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage :: Show a => a -> [Char] 
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage :: ClientMessage 
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

connectToServer :: HostName -> String -> IO Handle 
connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main :: IO () 
main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run :: Network.Socket.HostName -> String -> [Char] -> [Char] -> IO () 
run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  handleMessages h

handleMessages :: Handle -> IO () 
handleMessages h = do
  msg <- hGetLine h
  putStrLn $ "Received msg: " ++ msg
  case eitherDecode (L.pack msg) of
    Right serverMessage -> handleServerMessage h serverMessage
    Left err -> fail $ "Error parsing JSON: " ++ err

data ServerMessage = Join | GameInit GameInitData | CarPositions [CarPosition] | Unknown String

handleServerMessage :: Handle -> ServerMessage -> IO ()
handleServerMessage h serverMessage = do
  responses <- respond serverMessage
  forM_ responses $ hPutStrLn h
  handleMessages h

respond :: ServerMessage -> IO [ClientMessage]
respond message = case message of
  Join -> do
    putStrLn "Joined"
    return [pingMessage]
  GameInit gameInit -> do
    putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    return [pingMessage]
  CarPositions carPositions -> do
    return $ [throttleMessage 0.5]
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return [pingMessage]

instance FromJSON ServerMessage where
  parseJSON (Object v) = do
      msgType <- v .: "msgType" :: Parser Text
      case msgType of
        "join" -> return Join
        "gameInit" -> do
             msgData <- v .: "data"
             GameInit <$> parseJSON msgData
        "carPositions" -> do
             msgData <- v .: "data"
             CarPositions <$> parseJSON msgData
        _ -> return $ Unknown (show v)
